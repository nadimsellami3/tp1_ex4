# Exercice 4 TP1

## Objectif de l'exerice
L'objectif de cet exercice est de créer des packages permettant et de permettre la communication entre eux.

## Realisation 
J'ai créé deux packages, un permettant de stocker le script de SimpleCalculator et un autre package contenant un script de test.

## Organisation du projet
voici l'arborescence de notre projet à présent :

    .
    ├── Package_Calculator
    │   ├── Calculator.py
    │   ├── __init__.py
    │   └── __pycache__
    │       ├── Calculator.cpython-36.pyc
    │       └── __init__.cpython-36.pyc
    ├── Package_Test
    │   ├── exo_4.py
    │   └── __init__.py
    └── README.md

On voit donc bien les deux packages *Package_Calculator* et *Package_Test*, comportant notre classe SimpleCalculator dans le script Calculator.py, et notre script de test exo_4.py  

## Lancement du script
Si on essaie d'éxécuter le script exo_4.py, celui-ci ne vas pas fonctionner car il ne va pas être en mesure de trouver le package Package_Claculator.  
Il faut pour cela initialiser le PYHTONPATH à la racine du répertoire du projet pour qu'il puisse pointer sur les différents packages. Pour cela il faut entrer la commande :  
  
    export PYTHONPATH=$PYTHONPATH:'.'

puis on peut alors éxecuter le programme de test avec la commande :  

    python Package_Test/exo_4.py










